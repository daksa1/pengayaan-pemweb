<?php
 $update = false;
 $id_jadwal = 0;
 $id_dosen = 0;
 $id_kelas = 0;
 $jadwal= "";
 $matakuliah= "";

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "si_dosen";

        $conn = mysqli_connect($servername, $username, $password, $dbname);

        if($conn){
    
        }else{
            die("Connection failed : ".mysqli_connect_error());
        }

    if(isset($_POST["submit"])){
        $jadwal = $_POST["nama_kelas"];
        $matakuliah = $_POST["prodi"];

        $sql = "INSERT INTO `jadwal_kelas`(`id_jadwal`, `id_dosen`, `id_kelas`, `jadwal`, `matakuliah`) VALUES ('NULL','NULL','NULL','jadwal','matakuliah')";

        if(mysqli_query($conn, $sql)){
            $status = "Data Berhasil Ditambahkan";
        } else {
            $status = "Data Gagal Ditambahkan";
        }
    }
    if(isset($_GET["edit"])){
        $update = true;
        $id_jadwal= $_GET["edit"];
        $sql= "SELECT * FROM `jadwal_kelas` WHERE id_jadwal = $id_jadwal";
        $q1 = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($q1);

        $jadwal = $row["jadwal"];
        $matakuliah = $row["matakuliah"];
    }
    if(isset($_POST["edit"])){
        $id_jadwal = $_POST["id_jadwal"];
        $jadwal = $_POST["jadwal"];
        $matakuliah = $_POST["matakuliah"];

        $sql = "UPDATE `jadwal_kelas` SET `id_jadwal`='NULL',`id_dosen`='NULL',`id_kelas`='NULL',`jadwal`='$jadwal',`matakuliah`='$matakuliah' WHERE 1";

        if(mysqli_query($conn, $sql)){
            $status = "File Berhasil Diunggah";
        } else {
            $status = "File Gagal Diunggah";
        }
    }
    if(isset($_GET["delete"])){ 
        $id_jadwal = $_GET["delete"];
        $sql = "DELETE FROM `jadwal_kelas` WHERE id_jadwal = $id_jadwal";

        if(mysqli_query($conn, $sql)){
            $status = "File Berhasil Dihapus";
        } else {
            $status = "File Gagal Dihapus";
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Sistem Informasi Pengelolaan Data Dosen</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="index.php">Sistem Informasi</a>
                <a href="index.php"><img src="assets/img/home.png" alt="home" width ="50px"></a>
            </div>
        </nav>
        </header>
        <?php
                    include'connect.php';
                    $sql = "SELECT * FROM jadwal";
                    $result = mysqli_query($conn,$sql);
                ?>
        <?php
                include'connect.php';
                $sql = "SELECT * FROM `jadwal_kelas` INNER JOIN dosen ON dosen.id_dosen=jadwal_kelas.id_dosen INNER JOIN  kelas ON jadwal_kelas.id_kelas=kelas.id_kelas;";
                $result = mysqli_query($conn,$sql);
            ?>
        <section class="page-section portfolio" id="portfolio">
            <div class="container">
            <div class="row justify-content-center">
                <center><b><h1>Informasi Jadwal</h1></b></center>
                <table class="table table-dark table-striped" border="2" cellpadding="3">
                    <thead>
                        <tr>
                            <th>Id Jadwal</th>
                            <th>Nama Dosen</th>
                            <th>Kelas</th>
                            <th>Jadwal</th>
                            <th>Mata Kuliah</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php while($row = $result->fetch_assoc()): ?>
                        <tr>
                            <td><?php echo $row["id_jadwal"];?></td>
                            <td><?php echo $row["nama_dosen"];?></td>
                            <td><?php echo $row["nama_kelas"];?></td>
                            <td><?php echo $row["jadwal"];?></td>
                            <td><?php echo $row["matakuliah"];?></td>
                            <td>
                                <a href="kelola_jadwal.php?edit=<?php echo $row["id_kelas"];?>" class="btn btn-primary" >Edit</a>
                                <a href="jadwal.php?delete=<?php echo $row["id_kelas"];?>" class="btn btn-danger">Hapus</a>
                            </td>
                        </tr>
                    <?php endwhile;?>
                </table>
                <center>
                    <a href="kelola_jadwal.php" class="btn btn-primary" >Add Data</a>
                </center>
                </div>
                </div>
                <?php
                    function pre_r($array){
                        echo "<pre>";
                        print_r($array);
                        echo"<pre>";
                    }
                ?>
            </div>
        </section>
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright &copy; I Gusti Ngurah Daksa Hardistya</small></div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>