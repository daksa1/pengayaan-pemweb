<?php
 $update = false;
 $id = 0;
 $foto= "";
 $nama = "";
 $nip= "";
 $prodi="";
 $fakultas="";

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "si_dosen";

        $conn = mysqli_connect($servername, $username, $password, $dbname);

        if($conn){
    
        }else{
            die("Connection failed : ".mysqli_connect_error());
        }

    if(isset($_POST["submit"])){
        $nama = $_POST["nama_dosen"];
        $nip = $_POST["nip_dosen"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];

        if($_FILES["foto"]["error"] == 4){
            echo"Silahkan Upload Kembali";
        }else{
            $foto = $_FILES["foto_dosen"]["name"];
            $ambil = $_FILES["foto_dosen"]["tmp_name"] ;
            $tujuan = "img/".$foto;
            $move = move_uploaded_file($ambil,$tujuan);
        }

        $sql = "INSERT INTO `si_dosen`(`foto_dosen`, `nip_dosen`, `nama_dosen`, `prodi`, `fakultas`) VALUES ('$foto','$nip','$nama','$prodi','$fakultas')";

        if(mysqli_query($conn, $sql)){
            $status = "Data Berhasil Ditambahkan";
        } else {
            $status = "Data Gagal Ditambahkan";
        }
    }

    if(isset($_GET["edit"])){
        $update = true;
        $id= $_GET["edit"];
        $sql= "SELECT * FROM `dosen` WHERE id_dosen = $id";
        $q1 = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($q1);

        $nama = $row["nama_dosen"];
        $nip = $row["nip_dosen"];
        $prodi = $row["prodi"];
        $fakultas = $row["fakultas"];


        if($nip == " "){
            $status = "data kosong";
        }
    }

    if(isset($_POST["edit"])){
        $id = $_POST["id_dosen"];
        $nama = $_POST["nama_dosen"];
        $nip = $_POST["nip_dosen"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];

        if($_FILES["foto_dosen"]["error"] == 4){
            echo"Gawat Error Ne";
        }else{
            $foto = $_FILES["foto_dosen"]["name_dosen"];
            $ambil = $_FILES["foto_dosen"]["tmp_name"] ;
            $tujuan = "img/".$foto;
            $move = move_uploaded_file($ambil,$tujuan);
        }

        $sql = "UPDATE `dosen` SET `foto_dosen`='$foto',`nip_dosen`='$nip',`nama_dosen`='$nama',`prodi`='$prodi',`fakultas`='$fakultas' WHERE id_dosen = $id";

        if(mysqli_query($conn, $sql)){
            $status = "File Berhasil Diunggah";
        } else {
            $status = "File Gagal Diunggah";
        }
    }
    if(isset($_GET["delete"])){ 
        $id = $_GET["delete"];
        $sql = "DELETE FROM `dosen` WHERE id_dosen = $id";

        if(mysqli_query($conn, $sql)){
            $status = "File Berhasil Dihapus";
        } else {
            $status = "File Gagal Dihapus";
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Sistem Informasi Pengelolaan Data Dosen</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="index.php">Sistem Informasi</a>
                <a href="index.php"><img src="assets/img/home.png" alt="home" width ="50px"></a>
            </div>
        </nav>
        </header>
        <?php
                    include'connect.php';
                    $sql = "SELECT * FROM dosen";
                    $result = mysqli_query($conn,$sql);
                ?>
        <section class="page-section portfolio" id="portfolio">
            <div class="container">
            <div class="row justify-content-center">
                <center><b><h1>Informasi Dosen</h1></b></center>
                <table class="table table-dark table-striped" border="2" cellpadding="3">
                    <thead>
                        <tr>
                            <th>Foto Dosen</th>
                            <th>Nama Dosen</th>
                            <th>NIP</th>
                            <th>Prodi</th>
                            <th>Fakultas</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php while($row = $result->fetch_assoc()): ?>
                        <tr>
                            <td><img src="img/<?php echo $row["foto_dosen"];?>" width="80px" alt="Foto Dosen"></td>
                            <td><?php echo $row["nama_dosen"];?></td>
                            <td><?php echo $row["nip_dosen"];?></td>
                            <td><?php echo $row["prodi"];?></td>
                            <td><?php echo $row["fakultas"];?></td>
                            <td>
                                <a href="add_dosen.php?edit=<?php echo $row["id_dosen"];?>" class="btn btn-primary" >Edit</a>
                                <a href="dosen.php?delete=<?php echo $row["id_dosen"];?>" class="btn btn-danger">Hapus</a>
                            </td>
                        </tr>
                    <?php endwhile;?>
                </table>
                <center>
                    <a href="add_dosen.php" class="btn btn-primary" >Add Data</a>
                </center>
                </div>
                </div>
                <?php
                    function pre_r($array){
                        echo "<pre>";
                        print_r($array);
                        echo"<pre>";
                    }
                ?>
            </div>
        </section>
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright &copy; I Gusti Ngurah Daksa Hardistya</small></div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
