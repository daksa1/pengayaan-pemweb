<?php
 $update = false;
 $id = 0;
 $nama_kelas= "";
 $prodi= "";
 $fakultas= "";

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "si_dosen";

        $conn = mysqli_connect($servername, $username, $password, $dbname);

        if($conn){
    
        }else{
            die("Connection failed : ".mysqli_connect_error());
        }

    if(isset($_POST["submit"])){
        $nama_kelas = $_POST["nama_kelas"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];

        $sql = "INSERT INTO `kelas`(`id_kelas`, `nama_kelas`, `prodi`, `fakultas`) VALUES ('NULL','$nama_kelas','$prodi','$fakultas')";

        if(mysqli_query($conn, $sql)){
            $status = "Data Berhasil Ditambahkan";
        } else {
            $status = "Data Gagal Ditambahkan";
        }
    }
    if(isset($_GET["edit"])){
        $update = true;
        $id= $_GET["edit"];
        $sql= "SELECT * FROM `kelas` WHERE id_kelas = $id";
        $q1 = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($q1);

        $nama_kelas = $row["nama_kelas"];
        $prodi = $row["prodi"];
        $fakultas = $row["fakultas"];
    }

    if(isset($_POST["edit"])){
        $id = $_POST["id_kelas"];
        $nama_kelas = $_POST["nama_kelas"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];

        $sql = "UPDATE `kelas` SET `id_kelas`='[value-1]',`nama_kelas`='$nama_kelas',`prodi`='$prodi',`fakultas`='$fakultas' WHERE 1";

        if(mysqli_query($conn, $sql)){
            $status = "File Berhasil Diunggah";
        } else {
            $status = "File Gagal Diunggah";
        }
    }
    if(isset($_GET["delete"])){ 
        $id = $_GET["delete"];
        $sql = "DELETE FROM `kelas` WHERE id_kelas = $id";

        if(mysqli_query($conn, $sql)){
            $status = "File Berhasil Dihapus";
        } else {
            $status = "File Gagal Dihapus";
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Sistem Informasi Pengelolaan Data Dosen</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="index.php">Sistem Informasi</a>
                <a href="index.php"><img src="assets/img/home.png" alt="home" width ="50px"></a>
            </div>
        </nav>
        </header>
        <?php
                    include'connect.php';
                    $sql = "SELECT * FROM kelas";
                    $result = mysqli_query($conn,$sql);
                ?>
        <section class="page-section portfolio" id="portfolio">
            <div class="container">
            <div class="row justify-content-center">
                <center><b><h1>Informasi Kelas</h1></b></center>
                <table class="table table-dark table-striped" border="2" cellpadding="3">
                    <thead>
                        <tr>
                            <th>Id Kelas</th>
                            <th>Nama Kelas</th>
                            <th>Prodi</th>
                            <th>Fakultas</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <?php while($row = $result->fetch_assoc()): ?>
                        <tr>
                            <td><?php echo $row["id_kelas"];?></td>
                            <td><?php echo $row["nama_kelas"];?></td>
                            <td><?php echo $row["prodi"];?></td>
                            <td><?php echo $row["fakultas"];?></td>
                            <td>
                                <a href="kelola_kelas.php?edit=<?php echo $row["id_kelas"];?>" class="btn btn-primary" >Edit</a>
                                <a href="kelas.php?delete=<?php echo $row["id_kelas"];?>" class="btn btn-danger">Hapus</a>
                            </td>
                        </tr>
                    <?php endwhile;?>
                </table>
                <center>
                    <a href="kelola_kelas.php" class="btn btn-primary" >Add Data</a>
                </center>
                </div>
                </div>
                <?php
                    function pre_r($array){
                        echo "<pre>";
                        print_r($array);
                        echo"<pre>";
                    }
                ?>
            </div>
        </section>
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright &copy; I Gusti Ngurah Daksa Hardistya</small></div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
