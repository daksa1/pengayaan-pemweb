<?php
 $update = false;
 $id = 0;
 $foto = "";
 $nama = "";
 $nip = "";
 $prodi ="";
 $fakultas ="";

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "si_dosen";

        $conn = mysqli_connect($servername, $username, $password, $dbname);

        if($conn){
    
        }else{
            die("Connection failed : ".mysqli_connect_error());
        }

    if(isset($_POST["submit"])){
        $nama = $_POST["nama_dosen"];
        $nip = $_POST["nip_dosen"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];

        if($_FILES["foto_dosen"]["error"] == 4){
            echo"Silahkan Upload Kembali";
        }else{
            $foto = $_FILES["foto_dosen"]["name"];
            $ambil = $_FILES["foto_dosen"]["tmp_name"] ;
            $tujuan = "img/".$foto;
            $move = move_uploaded_file($ambil,$tujuan);
        }

        $sql = "INSERT INTO `dosen`(`id_dosen`, `foto_dosen`, `nip_dosen`, `nama_dosen`, `prodi`, `fakultas`) VALUES ('[value-1]','$foto','$nip','$nama','$prodi','$fakultas')";

        if(mysqli_query($conn, $sql)){
            $status = "Data Berhasil Ditambahkan";
        } else {
            $status = "Data Gagal Ditambahkan";
        }
    }

    if(isset($_GET["edit"])){
        $update = true;
        $id= $_GET["edit"];
        $sql= "SELECT * FROM `dosen` WHERE id_dosen = $id";
        $q1 = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($q1);

        $nama = $row["nama_dosen"];
        $nip = $row["nip_dosen"];
        $prodi = $row['prodi'];
        $fakultas = $row['fakultas'];


        if($nip == " "){
            $status = "data kosong";
        }
    }

    if(isset($_POST["edit"])){
        $id = $_POST["id_dosen"];
        $nama = $_POST["nama_dosen"];
        $nip = $_POST["nip_dosen"];
        $prodi = $_POST["prodi"];
        $fakultas = $_POST["fakultas"];

        if($_FILES["foto_dosen"]["error"] == 4){
            echo"Gawat Error Ne";
        }else{
            $foto = $_FILES["foto_dosen"]["name"];
            $ambil = $_FILES["foto_dosen"]["tmp_name"] ;
            $tujuan = "img/".$foto;
            $move = move_uploaded_file($ambil,$tujuan);
        }

        $sql = "UPDATE `dosen` SET `id_dosen`='[value-1]',`foto_dosen`='$foto',`nip_dosen`='$nip',`nama_dosen`='$nama',`prodi`='$prodi',`fakultas`='$fakultas' WHERE 1";

        if(mysqli_query($conn, $sql)){
            $status = "File Berhasil Diunggah";
        } else {
            $status = "File Gagal Diunggah";
        }
    }
    if(isset($_GET["delete"])){ 
        $id = $_GET["delete"];
        $sql = "DELETE FROM `dosen` WHERE id_dosen = $id";

        if(mysqli_query($conn, $sql)){
            $status = "File Berhasil Dihapus";
        } else {
            $status = "File Gagal Dihapus";
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Sistem Informasi Pengelolaan Data Dosen</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    </head>
    <body id="page-top">
        <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="#page-top">Sistem Informasi</a>
            </div>
        </nav>
        <section class="page-section portfolio" id="portfolio">
            <div class="container">
                    <div class="row justify-content-center">
                    <div class=" border-primary mt-3">
                        <div class="container p-3 my-3 bg-primary text-white">
                        <center>
                            <h1>Input Data Dosen</h1>
                            <p>Silahkan Masukkan Data Dosen</p>
                            </center>
                        </div>
            <form action="" method="post" enctype="multipart/form-data">
            <label for="Nama Dosen">Nama Dosen</label><br>
            <input type="text" class="form-control" name="nama_dosen" placeholder="Nama Dosen" id="nama_dosen" required>

            <label for="NIP">NIP</label><br>
            <input type="text" class="form-control" name="nip_dosen" placeholder="NIP" id="NIP" required>

            <label for="File">Foto</label><br>
            <input type="file" class="form-control" name="foto_dosen" id="foto_dosen" required>

            <label for="prodi">Program Studi</label><br>
                        <input list="prodi" class="form-control" placeholder="Program Studi" name="prodi">
                            <datalist id="prodi">
                                <option value="Sistem Informasi">
                                <option value="Teknologi Pendidikan">
                                <option value="Desain Komunikasi Visual">
                                <option value="Manajemen">
                                <option value="Survey dan Pemetaan">
                                <option value="Ilmu Keolahragaan">
                                <option value="Kedokteran">
                                <option value="Analisis Kimia">
                            </datalist>

                <label for="fakultas">Fakultas</label><br>
                        <input list="fakultas" class="form-control" placeholder="Fakultas" name="fakultas">
                            <datalist id="fakultas">
                                <option value="Fakultas Teknik dan Kejuruan">
                                <option value="Fakultas Ilmpu Pendidikan">
                                <option value="Fakultas Bahasa dan Seni">
                                <option value="Fakultas Ekonomi">
                                <option value="Fakultas Hukum dan Ilmu Sosial">
                                <option value="Fakultas Olahraga dan Kesehatan">
                                <option value="Fakultas Kedokteran">
                                <option value="Fakultas Matematika dan Ilmu Pengetahuan">
                            </datalist>
                            <br>

                        <div class="button mb5">
                            <?php if($update == true):?>
                                <input type="submit" class="btn btn-warning " name="edit" value="Ubah">
                                <?php else:?>
                                <input type="submit" class="btn btn-success " name="submit" value="Simpan">
                            <?php endif; ?>
                            <a href="dosen.php" class="btn btn-primary" >Back</a>
                        </div>
                        <div>
                            
                        </div>
                </form>
        </section>
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright &copy; I Gusti Ngurah Daksa Hardistya</small></div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>