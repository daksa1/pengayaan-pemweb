<?php
 $update = false;
 $id_jadwal = 0;
 $id_dosen = 0;
 $id_kelas = 0;
 $nama_dosen = "";
 $nama_kelas = "";
 $jadwal= "";
 $matakuliah= "";

        $servername = "localhost";
        $username = "root";
        $password = "";
        $dbname = "si_dosen";

        $conn = mysqli_connect($servername, $username, $password, $dbname);

        if($conn){
    
        }else{
            die("Connection failed : ".mysqli_connect_error());
        }

    if(isset($_POST["submit"])){
        $id_dosen = $_POST["id_dosen"];
        $id_kelas = $_POST["id_kelas"];
        $jadwal = $_POST["jadwal"];
        $matakuliah = $_POST["matakuliah"];

        $sql = "INSERT INTO `jadwal_kelas`(`id_jadwal`, `id_dosen`, `id_kelas`, `jadwal`, `matakuliah`) VALUES (NULL,'$id_dosen','$id_kelas','$jadwal','$matakuliah')";

        if(mysqli_query($conn, $sql)){
            $status = "Data Berhasil Ditambahkan";
        } else {
            $status = "Data Gagal Ditambahkan";
        }
    }
    if(isset($_GET["edit"])){
        $update = true;
        $id_jadwal= $_GET["edit"];
        $sql= "SELECT * FROM `jadwal_kelas` WHERE 1";
        $q1 = mysqli_query($conn, $sql);
        $row = mysqli_fetch_array($q1);
        $id_dosen = $row["id_dosen"];
        $id_kelas = $row["id_kelas"];
        $jadwal = $row["jadwal"];
        $matakuliah = $row["matakuliah"];
    }

    if(isset($_POST["edit"])){
        $id_dosen = $_POST["id_dosen"];
        $id_kelas = $_POST["id_kelas"];
        $jadwal = $_POST["jadwal"];
        $matakuliah = $_POST["matakuliah"];

        $sql = "UPDATE `jadwal_kelas` SET `id_jadwal`=NULL,`id_dosen`='$id_dosen',`id_kelas`='$id_kelas',`jadwal`='$jadwal',`matakuliah`='$matakuliah' WHERE 1";

        if(mysqli_query($conn, $sql)){
            $status = "File Berhasil Diunggah";
        } else {
            $status = "File Gagal Diunggah";
        }
    }
    if(isset($_GET["delete"])){ 
        $id_jadwal = $_GET["delete"];
        $sql = "DELETE FROM `jadwal_kelas` WHERE id_jadwal = $id_jadwal";

        if(mysqli_query($conn, $sql)){
            $status = "File Berhasil Dihapus";
        } else {
            $status = "File Gagal Dihapus";
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Sistem Informasi Pengelolaan Data Dosen</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Font Awesome icons (free version)-->
        <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" crossorigin="anonymous"></script>
        <!-- Google fonts-->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
        <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    </head>
    <body id="page-top">
        <!--<nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
            <div class="container">
                <a class="navbar-brand" href="index.php">Sistem Informasi</a>
                <a href="index.php"><img src="assets/img/home.png" alt="home" width ="50px"></a>
            </div>
        </nav>-->
        </header>
        <?php
                    include'connect.php';
                    $sql = "SELECT * FROM jadwal";
                    $result = mysqli_query($conn,$sql);
                ?>
        <section class="page-section portfolio" id="portfolio">
            <div class="container">
                    <div class="row justify-content-center">
                    <div class=" border-primary mt-3">
                        <div class="container p-3 my-3 bg-primary text-white">
                        <center>
                            <h1>Input Data Kelas</h1>
                            <p>Silahkan Masukkan Data Kelas</p>
                            </center>
                        </div>
            <form action="" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id_dosen" value="<?php echo $id_dosen; ?>">
                <?php
                    include 'connect.php';
                    $sql= "SELECT * FROM dosen";
                    $result = mysqli_query($conn,$sql);
                ?>
                <label for="Dosen">Nama Dosen</label><br>
                <select name="id_dosen" id="id_dosen" class='form-control' required>
                <option value="">Pilih Nama Dosen</option>
                    <?php while($row = $result->fetch_assoc()):?>
                    <option value="<?php echo $row["id_dosen"];?>"><?php echo $row["nama_dosen"]; ?></option>
                    <?php endwhile; ?>
                </select>
                <?php
                    include 'connect.php';
                    $sql= "SELECT * FROM kelas";
                    $result = mysqli_query($conn,$sql);
                ?>
                <label for="Dosen">Nama Kelas</label><br>
                <select name="id_kelas" id="id_kelas" class='form-control' required>
                <option value="">Pilih Nama Kelas</option>
                    <?php while($row = $result->fetch_assoc()):?>
                    <option value="<?php echo $row["id_kelas"];?>"><?php echo $row["nama_kelas"]; ?></option>
                    <?php endwhile; ?>
                </select>
            <label for="Jadwal">Jadwal</label><br>
            <input type="date" class="form-control" name="jadwal" placeholder="Jadwal" id="jadwal" required>
            <label for="Mata Kuliah">Mata Kuliah</label><br>
            <input type="text" class="form-control" name="matakuliah" placeholder="Mata Kuliah" id="matakuliah" required><br>
                        <div class="button mb5">
                            <?php if($update == true):?>
                                <input type="submit" class="btn btn-warning " name="edit" value="Ubah">
                                <?php else:?>
                                <input type="submit" class="btn btn-success " name="submit" value="Simpan">
                            <?php endif; ?>
                            <a href="jadwal.php" class="btn btn-primary" >Back</a>
                        </div>
                        <div>
                            
                        </div>
                </form>
        </section>
        <div class="copyright py-4 text-center text-white">
            <div class="container"><small>Copyright &copy; I Gusti Ngurah Daksa Hardistya</small></div>
        </div>
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <!-- * *                               SB Forms JS                               * *-->
        <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
        <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
        <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>
    </body>
</html>
